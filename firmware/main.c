/**
 * stereo-remote: an IR receiver to turn the Onkyo on/off.
 * 
 * Example transmission waveform for the Westinghouse remote:
 * ____          ____    _    _    ___    _    _    ___    _    ___
 *     \________/    \__/ \__/ \__/   \__/ \__/ \__/   \__/ \__/   \__...
 *         ST     SB     0    0     1     0    0     1     1     0
 *               ____    _    ___    _    _    _    ___    _    _    ____
 *           .../    \__/ \__/   \__/ \__/ \__/ \__/   \__/ \__/ \__/
 *                SB     0     1     0    0    0     1     0    0
 */

#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

/* Device pin definitions */
#define ONKYO_DDR  DDRC
#define ONKYO_PORT PORTC
#define ONKYO_BIT  5

#define IR_DDR DDRB
#define IR_PIN PINB 
#define IR_BIT 0

#define LED_DDR  DDRC
#define LED_PORT PORTC
#define LED_BIT  4

/* Pulse widths in us */
#define PW_ST 2400 // start transmission
#define PW_Sb 600  // separator between bits
#define PW_1  1200 // logical one
#define PW_0  600  // logical zero
#define PW_E  200  // allowable pulsewidth error

#define CODE_POWER 0xA9
#define CODE_ID    0x32
#define CODE_ERROR 0xFF

#define NUM_EDGES 26 // number of edges per transmission

#define STATE_IDLE    0
#define STATE_READING 1
#define STATE_READY   2


// keeps track of the current code reading state
volatile uint8_t state = STATE_IDLE;
// buffer for listing the timer counts at each edge
volatile uint16_t buf[NUM_EDGES];
// keeps track of the current edge index
volatile uint8_t edge_num = 0;


/*
 * Interrupt routine for handling a change in IR pin state.
 */
ISR(INT0_vect) {
    uint16_t count = TCNT1;

    switch (state) {
        case STATE_IDLE:
            TCNT1 = 0;
            state = STATE_READING;
            break;

        case STATE_READING:
            buf[edge_num++] = count;

            // if we've read all the edges for a transmission, we're done
            if (edge_num == NUM_EDGES) {
                edge_num = 0;
                state = STATE_READY;
            }
            break;
    }
}


/*
 * Initialize the pin change interrupt for the IR module.
 */
void ir_init(void) {
    // initialize the timer for 1 us tick length
    TCCR1B |= (1 << CS10); // no prescaler 

    // intialize the external interrupt on INT0
    EICRA |= (1 << ISC00); // set to interrupt on both logic changes
    EIMSK |= (1 << INT0); // enable the interrupt
}


/*
 * Decodes the buffer of edge timestamps into device ID and code bytes.
 */
uint8_t decode_buf(void) {
    uint8_t code = 0x00;
    uint8_t i;
    uint8_t j = 7;
    uint16_t diff = 0;

    for (i = 1; i < 16; i += 2) {
        diff = buf[i+1] - buf[i];

        if (diff > 1000) {
            // count as logical 1
            code |= (1 << j);
        }

        j--;
    }

    return code;
}


/*
 * Toggle the Onkyo power switch on then off.
 */
void onkyo_toggle(void) {
    ONKYO_PORT |= (1 << ONKYO_BIT);
    _delay_ms(100);
    ONKYO_PORT &= ~(1 << ONKYO_BIT);
}


int main(void) {
    uint8_t code;

    IR_DDR &= ~(1 << IR_BIT);
    ONKYO_DDR |= (1 << ONKYO_BIT);
    LED_DDR |= (1 << LED_BIT);
    
    ir_init();

    sei();

    for (;;) {
        if (state == STATE_READY) {
            cli();
            LED_PORT |= (1 << LED_BIT);
            
            code = decode_buf();

            if (code == CODE_POWER) {
                onkyo_toggle();
            }
            else if (code == CODE_ERROR) {
                // oh no, maybe blink a red LED?
            }
            
            _delay_ms(500);
            state = STATE_IDLE;
            sei();
        }
    }
}
